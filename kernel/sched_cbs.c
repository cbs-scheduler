static inline struct task_struct *cbs_task_of(struct sched_cbs_entity *se)
{
	return container_of(se, struct task_struct, cbs_se);
}

static inline struct rq *cbs_rq_of(struct cbs_rq *cbs_rq)
{
	return container_of(cbs_rq, struct rq, cbs);
}

#define for_each_cbs_sched_entity(se) \
		for (; se; se = NULL)

static inline struct cbs_rq *task_cbs_rq(struct task_struct *p)
{
	return &task_rq(p)->cbs;
}


static inline struct cbs_rq *cbs_cbs_rq_of(struct sched_cbs_entity *se)
{
	struct task_struct *p = cbs_task_of(se);
	struct rq *rq = task_rq(p);

	return &rq->cbs;
}

/* runqueue "owned" by this group */
static inline struct cbs_rq *group_cbs_rq(struct sched_cbs_entity *grp)
{
	return NULL;
}

static inline int
is_same_cbs_group(struct sched_cbs_entity *se, struct sched_cbs_entity *pse)
{
	return 1;
}

static inline struct sched_cbs_entity *parent_cbs_entity(struct sched_cbs_entity *se)
{
	return NULL;
}



/**************************************************************
 * Scheduling class tree data structure manipulation methods:
 */

static inline u64 max_dl(u64 min_dl, u64 dl)
{
	s64 delta = (s64)(dl - min_dl);
	if (delta > 0)
		min_dl = dl;

	return min_dl;
}

static inline u64 min_dl(u64 min_dl, u64 dl)
{
	s64 delta = (s64)(dl - min_dl);
	if (delta < 0)
		min_dl = dl;

	return min_dl;
}

static void
account_cbs_entity_dequeue(struct cbs_rq *cbs_rq, struct sched_cbs_entity *se);

static void
account_cbs_entity_enqueue(struct cbs_rq *cbs_rq, struct sched_cbs_entity *se);


static inline void deadline_postpone(struct sched_cbs_entity *cbs_se,u64 now);

static void __enqueue_cbs_entity(struct cbs_rq *cbs_rq, struct sched_cbs_entity *se);

static void enqueue_cbs_entity(struct cbs_rq *cbs_rq, struct sched_cbs_entity *se);

static void enqueue_task_cbs(struct rq *rq, struct task_struct *p, int wakeup);

static void cbs_deadline(struct sched_cbs_entity *se) {
	struct cbs_rq *cbs_rq=cbs_cbs_rq_of(se);
	printk("cbs_deadline! %p %p %lld\n",cbs_rq,se, se->budget);	
	//deadline_postpone(se);
	//se->budget=se->max_budget;
	//if (se != cbs_rq->curr){
	//if (se->budget < 0) {
	    //deadline_postpone(se);
	    //set_current_state(TASK_RUNNING);
	    //se->state=TASK_RUNNING;
	    //printk("cbs_deadline->");
	    while (se->budget<0)
		se->budget+=se->max_budget;
	    
	    __enqueue_cbs_entity(cbs_rq, se);
	    //printk("Incremento i task runnanti in cbs_deadline\n");
	    cbs_rq->nr_running++;

	    //account_cbs_entity_enqueue(cbs_rq,se);

	    se->on_rq=1;
	    	    //wake_up_process(cbs_task_of(se));
	    resched_task(cbs_task_of(se));
	//}
	//printk("cbs_deadline! %p %p\n",cbs_rq, se);
};

static enum hrtimer_restart cbs_end_deadline(struct hrtimer *timer)
{

	struct sched_cbs_entity *se = container_of(timer, struct sched_cbs_entity, cbs_deadline_timer);
	struct rq *rq = cbs_rq_of(cbs_cbs_rq_of(se));
	printk("timer has fired at deadline (%p)\n",se);
	//WARN_ON_ONCE(cpu_of(rq) != smp_processor_id());

	spin_lock(&rq->lock);
	update_rq_clock(rq);
	/**
        Qua va inserita una funzione che inserisce il task 
	nel rbtree e ricarica il budget (postpone_deadline + enqueue_task)
	Question: I parametri qua son corretti? 
	rq->curr punta al task cbs che contiene il timer espirato?
	**/
	cbs_deadline(se);
	
	//rq->curr->sched_class->end_(rq, rq->curr, 1);
	spin_unlock(&rq->lock);
	//printk("timer has fired at deadline end(%p)\n",se);

	return HRTIMER_NORESTART;
}




static void init_cbs_timer(struct sched_cbs_entity *cbs_se){
	//printk("init_cbs_timer %p\n",cbs_se);
	struct cbs_rq *cbs_rq = cbs_cbs_rq_of(cbs_se);
	hrtimer_init(&cbs_se->cbs_deadline_timer, CLOCK_MONOTONIC, HRTIMER_MODE_ABS);
	cbs_se->cbs_deadline_timer.function = cbs_end_deadline;
	cbs_se->cbs_deadline_timer.irqsafe = 1;
		static int warn;

       if (!hrtick_enabled(cbs_rq_of(cbs_rq))) {
               if (!warn) {
                       warn = 1;
                       printk("Warning HRTICKs disabled!\n");
               }
               //return;
       }
	//printk("init_cbs_timer end %p\n",cbs_se);
	
}


static void start_cbs_timer(struct sched_cbs_entity *cbs_se)
{
	//printk("start_cbs_timer %p\n",cbs_se);
	u64 now;
	struct cbs_rq *cbs_rq = cbs_cbs_rq_of(cbs_se);
        
	//ktime_t now;
	
	/*	if (!rt_bandwidth_enabled() || rt_b->rt_runtime == RUNTIME_INF)
		return;
*/
	now = cbs_rq_of(cbs_rq)->clock;
	if (now > cbs_se->deadline)
	   hrtimer_cancel(&cbs_se->cbs_deadline_timer);


	if (hrtimer_active(&cbs_se->cbs_deadline_timer))
		return;
	deadline_postpone(cbs_se,now);
	   
	
	WARN_ON(now>cbs_se->deadline);
	//spin_lock(&cbs_se->cbs_sleeptime_lock);
	for (;;) {
		ktime_t hard;
		
		if (hrtimer_active(&cbs_se->cbs_deadline_timer))
					break;
		now = cbs_rq_of(cbs_rq)->clock;

		//now = hrtimer_cb_get_time(&cbs_se->cbs_deadline_timer);
		//if (cbs_se->deadline==0)
		//	cbs_se->deadline=now+cbs_se->period;
		//printk("DEAD setted to set=%llu",(unsigned long long)cbs_se->deadline);
	//printk(" PERIOD %llu\n",(unsigned long long)cbs_se->period);
		printk("At time %llu I set the new deadline %llu\n",now,cbs_se->deadline);
		//printk("Dati: %llu %llu %p\n",now,cbs_se->period, cbs_se);
		//printk("Timer settato a %llu\n",cbs_se->deadline);
	//	hrtimer_forward(&cbs_se->cbs_deadline_timer, now, ns_to_ktime(cbs_se->period));

		//soft = hrtimer_get_softexpires(&cbs_se->cbs_deadline_timer);
		//hard = hrtimer_get_expires(&cbs_se->cbs_deadline_timer);
		//delta = ktime_to_ns(ktime_sub(hard, soft));
		hard=ns_to_ktime(cbs_se->deadline);
		__hrtimer_start_range_ns(&cbs_se->cbs_deadline_timer, hard, 0, HRTIMER_MODE_ABS, 0);
		//hrtimer_start(&cbs_se->cbs_deadline_timer, hard, HRTIMER_MODE_ABS);
	}
	//spin_unlock(&cbs_se->cbs_sleeptime_lock); 
	//printk("start_cbs_timer end%p\n",cbs_se);

}



static inline void deadline_postpone(struct sched_cbs_entity *cbs_se,u64 now)
{
	//printk("deadline_postpone %p\n",cbs_se);
	//while (cbs_se->budget < 0) {
	while (cbs_se->deadline<now)
		cbs_se->deadline += cbs_se->period;
		//cbs_se->budget += cbs_se->max_budget;
	//}
	//printk("deadline_postpone end %p\n",cbs_se);

	
}

static inline s64 entity_deadline(struct cbs_rq *cbs_rq, struct sched_cbs_entity *se)
{
	return se->deadline - cbs_rq->min_deadline;
}

/*
 * Enqueue an entity into the rb-tree:
 */
static void __enqueue_cbs_entity(struct cbs_rq *cbs_rq, struct sched_cbs_entity *se)
{
	//printk("__enqueue_cbs_entity %p %p\n",cbs_rq,se);

	struct rb_node **link = &cbs_rq->tasks_timeline.rb_node;
	struct rb_node *parent = NULL;
	struct sched_cbs_entity *entry;
	s64 key = entity_deadline(cbs_rq, se);
	int leftmost = 1;

	/*
	 * Find the right place in the rbtree:
	 */
	while (*link) {
		parent = *link;
		entry = rb_entry(parent, struct sched_cbs_entity, run_node);
		/*
		 * We dont care about collisions. Nodes with
		 * the same key stay together.
		 */
		if (key < entity_deadline(cbs_rq, entry)) {
			link = &parent->rb_left;
		} else {
			link = &parent->rb_right;
			leftmost = 0;
		}
	}
	/*
	 * Maintain a cache of leftmost tree entries (it is frequently
	 * used):
	 */
	if (leftmost) {
		cbs_rq->rb_leftmost = &se->run_node;
		/*
		 * maintain cbs_rq->min_deadline to be a monotonic increasing
		 * value tracking the leftmost deadline in the tree.
		 */
		cbs_rq->min_deadline =
			max_dl(cbs_rq->min_deadline, se->deadline);
	}
	//printk("Prima di inserire il nodo %p %p %p\n",&se->run_node,parent,link);	
	BUG_ON(parent!=NULL);
	rb_link_node(&se->run_node, parent, link);
	rb_insert_color(&se->run_node, &cbs_rq->tasks_timeline);
	//printk("__enqueue_cbs_entity end %p %p\n",cbs_rq,se);

}

static void __dequeue_cbs_entity(struct cbs_rq *cbs_rq, struct sched_cbs_entity *se)
{
	//printk("__dequeue_cbs_entity %p\n",se);

	if (cbs_rq->rb_leftmost == &se->run_node) {
		struct rb_node *next_node;
		struct sched_cbs_entity *next;

		next_node = rb_next(&se->run_node);
		cbs_rq->rb_leftmost = next_node;

		if (next_node) {
			next = rb_entry(next_node,
					struct sched_cbs_entity, run_node);
			cbs_rq->min_deadline =
				max_dl(cbs_rq->min_deadline,
					     next->deadline);
		}
	}

	rb_erase(&se->run_node, &cbs_rq->tasks_timeline);
//	printk("__dequeue_cbs_entity end%p\n",se);
}

static inline struct rb_node *earliest_deadline(struct cbs_rq *cbs_rq)
{
	return cbs_rq->rb_leftmost;
}

static struct sched_cbs_entity *__pick_next_cbs_entity(struct cbs_rq *cbs_rq)
{
	//printk("Entro in pick_next_cbs_entity con %ld\n",cbs_rq->nr_running);
	//printk("Ritorno l'entity cbs %p\n",rb_entry(earliest_deadline(cbs_rq), struct sched_cbs_entity, run_node));
	return rb_entry(earliest_deadline(cbs_rq), struct sched_cbs_entity, run_node);
}

/*
 * Update the current task's runtime statistics. Skip current tasks that
 * are not in our scheduling class.
 */
static inline void
__update_curr_cbs(struct cbs_rq *cbs_rq, struct sched_cbs_entity *curr,
	      unsigned long delta_exec)
{
	//printk("__update_curr_cbs %p %p\n",cbs_rq,curr);
        //ktime_t t;
        //struct task_struct *p=cbs_task_of(curr);
	//schedstat_set(curr->exec_max, max((u64)delta_exec, curr->exec_max));

//	curr->sum_exec_runtime += delta_exec;
	//schedstat_add(cbs_rq, exec_clock, delta_exec);
	curr->budget -= delta_exec;
	//printk("budget del task %p is %lld\n",curr,curr->budget);
	/**Soft CBS: If budget < 0 recharge buffer and postpone deadline**/
	/**Hard CBS: If budget < 0 start a timer until the deadline**/
#ifdef CBS_SOFT
	BUG_ON(cbs_rq->curr<=0);
#endif
	if (curr->budget < 0)
	  {
	   printk("Budget ended for task %p\n",curr);
   	   //deadline_postpone(curr);	
#ifndef CBS_SOFT
	  start_cbs_timer(curr);
#else
	  deadline_postpone(curr);
#endif
	  //printk("Provo a dormire %ld %d\n",cbs_rq->nr_running,curr->on_rq);
	  //sleeping_task = current;
	  //set_current_state(TASK_UNINTERRUPTIBLE);
	  //printk("Decremento i task runnanti in __update_curr_cbs\n");
	  cbs_rq->nr_running--;
	  curr->on_rq=0;
	  
	  //account_cbs_entity_dequeue(cbs_rq,curr);
	  //schedule();

	  //rb_erase(&curr->run_node, &cbs_rq->tasks_timeline);
	  //p->state=TASK_UNINTERRUPTIBLE;
	  //t=ns_to_ktime(curr->deadline);
	  //schedule_hrtimeout(&t,HRTIMER_MODE_ABS);
	   //curr->on_rq = 0;
	   //printk("Current: %i\n",curr->on_rq); 
	   resched_task(cbs_task_of(curr));
	  }
	//printk("__update_curr_cbs end %p %p\n",cbs_rq,curr);
	
}

static void update_curr_cbs(struct cbs_rq *cbs_rq)
{
	//printk("update_curr_cbs %p\n",cbs_rq);
	//printk("cbs_rq %p\n",cbs_rq);
	struct sched_cbs_entity *curr = cbs_rq->curr;
	u64 now = cbs_rq_of(cbs_rq)->clock;
	unsigned long delta_exec;
	//BUG_ON(curr->on_rq==0);

	if (unlikely(!curr))
		return;

	/*
	 * Get the amount of time the current task was running
	 * since the last accounting time
	 */
	delta_exec = (unsigned long)(now - curr->exec_start);

	__update_curr_cbs(cbs_rq, curr, delta_exec);
	curr->exec_start = now;

#if 0
	if (entity_is_task(curr)) {
		struct task_struct *curtask = cbs_task_of(curr);

		cpuacct_charge(curtask, delta_exec);
	}
#endif
	//printk("update_curr_cbs end%p\n",cbs_rq);

}

/*
 * We are picking a new current task - update its stats:
 */
static inline void
update_stats_curr_start_cbs(struct cbs_rq *cbs_rq, struct sched_cbs_entity *se)
{
	/*
	 * We are starting a new run period:
	 */
	se->exec_start = cbs_rq_of(cbs_rq)->clock;
}

/**************************************************
 * Scheduling class queueing methods:
 */

static void
account_cbs_entity_enqueue(struct cbs_rq *cbs_rq, struct sched_cbs_entity *se)
{
	//printk("account_cbs_entity_enqueue %p %p\n",cbs_rq,se);
	//printk("Incremento i task runnanti in account_cbs_entity_enqueue\n");
	cbs_rq->nr_running++;
	se->on_rq = 1;
	//printk("account_cbs_entity_enqueue end %p %p\n",cbs_rq,se);
	
}

static void
account_cbs_entity_dequeue(struct cbs_rq *cbs_rq, struct sched_cbs_entity *se)
{
	//printk("Decremento i task runnanti in account_cbs_entity_dequeue\n");

	//printk("account_cbs_entity_dequeue %p %p\n",cbs_rq,se);
	BUG_ON(se->on_rq == 0);
	BUG_ON(cbs_rq->nr_running == 0);
	cbs_rq->nr_running--;
	se->on_rq = 0;
	//printk("account_cbs_entity_dequeue end%p %p\n",cbs_rq,se);
}

static void
enqueue_cbs_entity(struct cbs_rq *cbs_rq, struct sched_cbs_entity *se)
{
	u64 vt, now = cbs_rq_of(cbs_rq)->clock;
	//printk("enqueue_cbs_entity %p %p\n",cbs_rq,se);

	/*
	 * Update run-time statistics of the 'current'.
	 */
	//printk("from enqueue_cbs_entity\n");
	update_curr_cbs(cbs_rq);
	account_cbs_entity_enqueue(cbs_rq, se);

	vt = se->period * se->budget;
	do_div(vt, se->max_budget);
//	printk("dead %llu and budget %lld\n",(unsigned long long)se->deadline,(long long)se->budget);
	if (vt + now > se->deadline) {
		se->budget = se->max_budget;
		se->deadline = se->period + now;
	}
//	printk("dead %llu and budget %lld\n",(unsigned long long)se->deadline,(long long)se->budget);

/**HARD: If budget < 0 does not enqueue the current task**/
/**SOFT: Enqueue the task beacuse budget can not be <= 0**/
	//printk("Budget %lld\n",se->budget);
	if ((se != cbs_rq->curr)&&(se->budget > 0)){
	   //printk("Enqueue_cbs_entity->");
	   __enqueue_cbs_entity(cbs_rq, se);
	  }
	//printk("enqueue_cbs_entity end%p %p\n",cbs_rq,se);

}

static void
dequeue_cbs_entity(struct cbs_rq *cbs_rq, struct sched_cbs_entity *se)
{
	/*
	 * Update run-time statistics of the 'current'.
	 */
	//printk("from dequeue_cbs_entity\n");
	update_curr_cbs(cbs_rq);

	if (se != cbs_rq->curr){
		//printk("dequeue_cbs_entity->");
		__dequeue_cbs_entity(cbs_rq, se);
	}
	account_cbs_entity_dequeue(cbs_rq, se);
	//printk("dequeue_cbs_entity end %p\n",se);

}

static void
set_next_cbs_entity(struct cbs_rq *cbs_rq, struct sched_cbs_entity *se)
{
	/* 'current' is not kept within the tree. */
	if (se->on_rq) {
		//printk("set_next_cbs_entity->");
		__dequeue_cbs_entity(cbs_rq, se);
	}

	update_stats_curr_start_cbs(cbs_rq, se);
	cbs_rq->curr = se;
//	se->prev_sum_exec_runtime = se->sum_exec_runtime;
}

static int
wakeup_preempt_cbs_entity(struct sched_cbs_entity *curr, struct sched_cbs_entity *se)
{
	return se->deadline < curr->deadline;
}


static struct sched_cbs_entity *pick_next_cbs_entity(struct cbs_rq *cbs_rq)
{
	
	struct sched_cbs_entity *se = NULL;
	//printk("pick_next_cbs_entity\n");

	if (earliest_deadline(cbs_rq)) {
		se = __pick_next_cbs_entity(cbs_rq);
		set_next_cbs_entity(cbs_rq, se);
	}
//	printk("pick_next_cbs_entity end %p\n",se);

	return se;
}

static void put_prev_cbs_entity(struct cbs_rq *cbs_rq, struct sched_cbs_entity *prev)
{
	//printk("put_prev_cbs_entity %p\n",prev);

	/*
	 * If still on the runqueue then deactivate_task()
	 * was not called and update_curr() has to be done:
	 */
	if (prev->on_rq) {
		//printk("from put_prev_cbs_entity\n");
		update_curr_cbs(cbs_rq);
	}

	if ((prev->on_rq)&&(prev->budget>0)) {
		/* Put 'current' back into the tree. */
	        //printk("Put_prev_cbs_entity->");
		__enqueue_cbs_entity(cbs_rq, prev);
		//cbs_rq->nr_running++;
	}
	cbs_rq->curr = NULL;
	//printk("put_prev_cbs_entity end %p\n",prev);
	
}

static void
cbs_entity_tick(struct cbs_rq *cbs_rq, struct sched_cbs_entity *curr, int queued)
{
	/*
	 * Update run-time statistics of the 'current'.
	 */
	//printk("from cbs_entity_tick\n");
	if (curr->on_rq)
	   update_curr_cbs(cbs_rq);

	//if (cbs_rq->nr_running > 1)
	resched_task(cbs_rq_of(cbs_rq)->curr);	/* FIXME: Check! */
}


/**************************************************
 * CBS operations on tasks:
 */

#ifdef CONFIG_SCHED_HRTICK
static void hrtick_start_cbs(struct rq *rq, struct task_struct *p)
{
//	int requeue = rq->curr == p;
	//printk("hrtick_start_cbs %p\n",p);
	struct sched_cbs_entity *se = &p->cbs_se;
	//struct cbs_rq *cbs_rq = cbs_cbs_rq_of(se);
	s64 delta;

	WARN_ON(task_rq(p) != rq);
	BUG_ON(se->budget<=0);
	/*if (se->budget <= 0)
	  {
	   printk("budget terminato per il task %p\n",se);
	   //se->budget=0;
	   //deadline_postpone(se);
	   //start_cbs_timer(se);
	   //rq->curr=NULL;
	   if (rq->curr == p)
	   	resched_task(p);
	   return;
	  }*/
	/*
	 * Don't schedule timeouts shorter than 10000ns, that just
	 * doesn't make sense.
	 */
	delta = max(10000LL, se->budget);
	//printk("hrtick start %p cn delta pari a %lld\n",p,delta);

	hrtick_start(rq, delta);
	//printk("hrtick_start_cbs end cn budget pari %lld\n",se->budget);
	
}
#else
static inline void
hrtick_start_cbs(struct rq *rq, struct task_struct *p, s64 time)
{
}
#endif

/*
 * The enqueue_task method is called before nr_running is
 * increased. Here we update the fair scheduling stats and
 * then put the task into the rbtree:
 */
static void enqueue_task_cbs(struct rq *rq, struct task_struct *p, int wakeup)
{
	
	struct cbs_rq *cbs_rq;
	struct sched_cbs_entity *se = &p->cbs_se;
	//printk("enqueue_task_cbs %p\n",se);

	for_each_cbs_sched_entity(se) {
		if (se->on_rq)
			break;
		cbs_rq = cbs_cbs_rq_of(se);
		enqueue_cbs_entity(cbs_rq, se);
	}

	//hrtick_start_cbs(rq, rq->curr);
	//printk("enqueue_task_cbs end%p\n",se);
}

/*
 * The dequeue_task method is called before nr_running is
 * decreased. We remove the task from the rbtree and
 * update the fair scheduling stats:
 */
static void dequeue_task_cbs(struct rq *rq, struct task_struct *p, int sleep)
{

	struct cbs_rq *cbs_rq;
	struct sched_cbs_entity *se = &p->cbs_se;

	//printk("dequeue_task_cbs %p\n",se);

	for_each_cbs_sched_entity(se) {
		cbs_rq = cbs_cbs_rq_of(se);
		dequeue_cbs_entity(cbs_rq, se);
		/* FIXME: Don't dequeue parent if it has other entities besides us */
	}

	//hrtick_start_cbs(rq, rq->curr);
	//printk("dequeue_task_cbs end%p\n",se);
	
}

/*
 * sched_yield() is broken on CBS.
 *
 * If compat_yield is turned on then we requeue to the end of the tree.
 */
static void yield_task_cbs(struct rq *rq)
{
}

/* return depth at which a sched entity is present in the hierarchy */
static inline int depth_se_cbs(struct sched_cbs_entity *se)
{
	int depth = 0;

	for_each_cbs_sched_entity(se)
		depth++;

	return depth;
}

/*
 * Preempt the current task with a newly woken task if needed:
 */
static void check_preempt_wakeup_cbs(struct rq *rq, struct task_struct *p,int syn)
{
	struct task_struct *curr = rq->curr;
	struct cbs_rq *cbs_rq = task_cbs_rq(curr);
	struct sched_cbs_entity *se = &curr->cbs_se, *pse = &p->cbs_se;
#if 0
	int se_depth, pse_depth;
#endif
	//printk("check_preempt_wakeup_cbs %p %p\n",cbs_rq,se);

	if (unlikely(rt_prio(p->prio))) {
		update_rq_clock(rq);
		//printk("from check_preempt_wakeup\n");
		update_curr_cbs(cbs_rq);
		resched_task(curr);
		return;
	}

//	se->last_wakeup = se->sum_exec_runtime;
	if (unlikely(se == pse))
		return;

#if 0
/*
	 * preemption test can be made between sibling entities who are in the
	 * same cbs_rq i.e who have a common parent. Walk up the hierarchy of
	 * both tasks until we find their ancestors who are siblings of common
	 * parent.
	 */

	/* First walk up until both entities are at same depth */
	se_depth = depth_se_cbs(se);
	pse_depth = depth_se_cbs(pse);

	while (se_depth > pse_depth) {
		se_depth--;
		se = parent_cbs_entity(se);
	}

	while (pse_depth > se_depth) {
		pse_depth--;
		pse = parent_cbs_entity(pse);
	}

	while (!is_same_cbs_group(se, pse)) {
		se = parent_cbs_entity(se);
		pse = parent_cbs_entity(pse);
	}
#endif
	if (wakeup_preempt_cbs_entity(se, pse) == 1)
		resched_task(curr);
}

static struct task_struct *pick_next_task_cbs(struct rq *rq)
{
	
	struct task_struct *p;
	struct cbs_rq *cbs_rq = &rq->cbs;
	struct sched_cbs_entity *se;
	//printk("pick_next_task_cbs %lu\n",cbs_rq->nr_running);
	if (unlikely(!cbs_rq->nr_running))
		return NULL;

	do {
		se = pick_next_cbs_entity(cbs_rq);
		cbs_rq = group_cbs_rq(se);
	} while (cbs_rq);

	p = cbs_task_of(se);
	//if (likely(se!=NULL))
	hrtick_start_cbs(rq, p);
	//printk("pick_next_task_cbs end%p\n",se);
	return p;
}

/*
 * Account for a descheduled task:
 */
static void put_prev_task_cbs(struct rq *rq, struct task_struct *prev)
{
	struct sched_cbs_entity *se = &prev->cbs_se;
	struct cbs_rq *cbs_rq;
	//printk("put_prev_task_cbs\n");

	for_each_cbs_sched_entity(se) {
		cbs_rq = cbs_cbs_rq_of(se);
		put_prev_cbs_entity(cbs_rq, se);
	}
	//printk("put_prev_task_cbs end\n");

}

/*
 * scheduler tick hitting a task of our scheduling class:
 */
static void task_tick_cbs(struct rq *rq, struct task_struct *curr, int queued)
{
	struct cbs_rq *cbs_rq;
	struct sched_cbs_entity *se = &curr->cbs_se;

	for_each_cbs_sched_entity(se) {
		cbs_rq = cbs_cbs_rq_of(se);
		cbs_entity_tick(cbs_rq, se, queued);
	}
}

/*
 * FIXME!
 */
static void task_new_cbs(struct rq *rq, struct task_struct *p)
{
//#warning Task New CBS is W R O N G ! ! !
	struct cbs_rq *cbs_rq = task_cbs_rq(p);
	//printk("from task_new_cbs \n");
	sched_info_queued(p);
	
	update_curr_cbs(cbs_rq);
//#ifndef CBS_SOFT
	init_cbs_timer(cbs_rq->curr);
//#endif
	enqueue_task_cbs(rq, p, 0);
	resched_task(rq->curr);
}

/*
 * Priority of the task has changed. Check to see if we preempt
 * the current task.
 */
static void prio_changed_cbs(struct rq *rq, struct task_struct *p,
			      int oldprio, int running)
{
//#warning Check prio_changed_cbs() implementation, thanks!
	//printk("prio_changed_cbs has been called!\n");
	check_preempt_curr(rq, p,running);
}

/*
 * We switched to the sched_cbs class.
 */
static void switched_to_cbs(struct rq *rq, struct task_struct *p,
			     int running)
{
//#warning Check switched_to_cbs() implementation, thanks!
	//printk("switched_to_cbs has been called!\n");
	check_preempt_curr(rq, p, running);
}

/* Account for a task changing its policy or group.
 *
 * This routine is mostly called to set cbs_rq->curr field when a task
 * migrates between groups/classes.
 */
static void set_curr_task_cbs(struct rq *rq)
{
	struct sched_cbs_entity *se = &rq->curr->cbs_se;

	for_each_cbs_sched_entity(se)
		set_next_cbs_entity(cbs_cbs_rq_of(se), se);
}

/*
 * All the scheduling class methods:
 */
static const struct sched_class cbs_sched_class = {
	.next			= &fair_sched_class,
	.enqueue_task		= enqueue_task_cbs,
	.dequeue_task		= dequeue_task_cbs,
	.yield_task		= yield_task_cbs,
#ifdef CONFIG_SMP
#error CBS SMP is still a No-No!
	.select_task_rq		= ,
#endif /* CONFIG_SMP */

	.check_preempt_curr	= check_preempt_wakeup_cbs,

	.pick_next_task		= pick_next_task_cbs,
	.put_prev_task		= put_prev_task_cbs,

#ifdef CONFIG_SMP
#error CBS SMP is still a No-No!
	.load_balance		= ,
	.move_one_task		= ,
#endif

	.set_curr_task          = set_curr_task_cbs,
	.task_tick		= task_tick_cbs,
	.task_new		= task_new_cbs,

	.prio_changed		= prio_changed_cbs,
	.switched_to		= switched_to_cbs,

#ifdef CONFIG_CBS_GROUP_SCHED
	.moved_group		= ,
#endif
};


